### Multi stage docker build
This example shows a way how to have Docker multistage builds with intermediate artifact copy.
NOTES:

1. There are two different images used in this example. One is a standard Golang image from the docker hub, golang:1.9.5-stretch. Changes to this image discarded in the end of the process. The second one is minimal alpine image with bash installed(haven't found a way to force packer to use /bin/sh for file provisioner). Artifact of the build transferred to this image and updated image committed.     
2. There are constrains in place for provisioners. It's based on names of builders(images).
`"name": "builder",`
So provisioners will be executed only on allowed builders.`"only": ["builder"]`
3. Build artifact being copied from build container to the host and from the host to "packer" container. As far as I know there's no way to do it from image to image directly with packer.
4. It's possible to create general build and test pipeline with packer and override default variables with variable file.
